package com.example.navigation_demo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.navigation.fragment.findNavController

class FragmentSpecifyAmount : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_specify_amount,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val inpText = view.findViewById<EditText>(R.id.amount)
        val btnSend = view.findViewById<Button>(R.id.buttonSend)
        val btnCancel = view.findViewById<Button>(R.id.cancel_button1)
        btnSend.setOnClickListener {
            val amount = inpText.text.toString()
            val name = FragmentSpecifyAmountArgs.fromBundle(requireArguments()).Name
            if(amount.isEmpty()){
                inpText.error = "Please enter a name"
            }else{
                val action = FragmentSpecifyAmountDirections.actionFragmentSpecifyAmountToFragmentConfirmation(name,amount.toFloat())
                findNavController().navigate(action)
            }
        }
        btnCancel.setOnClickListener {
            findNavController().navigate(R.id.fragmentMain)
        }
    }
}
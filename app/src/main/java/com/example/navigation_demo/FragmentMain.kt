package com.example.navigation_demo

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController

class FragmentMain : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val btnViewTransaction = view.findViewById<Button>(R.id.view_transaction)
        val btnSendMoney = view.findViewById<Button>(R.id.send_money)
        val btnViewBalance = view.findViewById<Button>(R.id.view_balance)

        btnViewTransaction.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentMain_to_fragmentViewTransaction)
        }
        btnSendMoney.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentMain_to_fragmentChooseRecipient)
        }
        btnViewBalance.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentMain_to_fragmentViewBalance)
        }
    }
    override fun onStart() {
        super.onStart()
        Log.d(TAG,"Fragment 1 onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"Fragment 1 onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG,"Fragment 1 onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"Fragment 1 onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"Fragment 1 onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG,"Fragment 1 onCreate")
    }
}
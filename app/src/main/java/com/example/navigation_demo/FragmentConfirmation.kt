package com.example.navigation_demo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.fragment.findNavController

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FragmentConfirmation.newInstance] factory method to
 * create an instance of this fragment.
 */
class FragmentConfirmation : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_confirmation,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name = FragmentConfirmationArgs.fromBundle(requireArguments()).Name.toString()
        val amount = FragmentConfirmationArgs.fromBundle(requireArguments()).Amount.toString()
        view.findViewById<TextView>(R.id.sent).text = "\$$amount"
        view.findViewById<TextView>(R.id.recipient).text = "was sent to $name"
        view.findViewById<Button>(R.id.button_finish).setOnClickListener {
            findNavController().navigate(R.id.fragmentMain)
        }
    }

}
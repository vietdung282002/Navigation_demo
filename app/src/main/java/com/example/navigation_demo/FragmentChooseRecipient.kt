package com.example.navigation_demo

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.navigation.fragment.findNavController

class FragmentChooseRecipient : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_choose_recipient,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val inpText = view.findViewById<EditText>(R.id.name)
        val btnNext = view.findViewById<Button>(R.id.buttonNext)
        val btnCancel = view.findViewById<Button>(R.id.cancel_button)
        btnNext.setOnClickListener {
            val name = inpText.text.toString()
            if(name.isEmpty()){
                inpText.error = "Please enter a name"
            }else{
                val action = FragmentChooseRecipientDirections.actionFragmentChooseRecipientToFragmentSpecifyAmount(name)
                findNavController().navigate(action)
            }
        }
        btnCancel.setOnClickListener {
            findNavController().navigate(R.id.fragmentMain)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG,"Fragment 3 onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"Fragment 3 onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG,"Fragment 3 onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"Fragment 3 onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"Fragment 3 onDestroy")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG,"Fragment 3 onCreate")
    }


}